import { Component, OnInit } from '@angular/core';
import { DailyCallReportService } from '../services/daily-call-report.service';
import { Doctor } from '../model/doctor';
import { ModalController } from '@ionic/angular';
import { AddDoctorPage } from '../add-doctor/add-doctor.page';

@Component({
  selector: 'app-doctor',
  templateUrl: './doctor.page.html',
  styleUrls: ['./doctor.page.scss'],
})
export class DoctorPage implements OnInit {

  private doctorList: Array<Doctor>;

  constructor(
    private DCRService: DailyCallReportService,
    private modalController: ModalController,
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.loadDoctorList();
  }

  loadDoctorList() {
    this.DCRService.getDoctorList()
      .subscribe({
        next: (res) => {
          this.DCRService.doctorList = [];
          res.data.forEach((doctor) => {
            let tempDoctor = new Doctor();
            tempDoctor.name = doctor.doctor_name;
            tempDoctor.area = doctor.area;
            tempDoctor.place = doctor.sub_territory;
            tempDoctor.serverID = doctor.name;
            this.DCRService.doctorList.push(tempDoctor);
          });
          this.doctorList = this.DCRService.doctorList;
        }
      })
  }

  async addDoctor() {
    let doctor = new Doctor();
    const modal = await this.modalController.create({
      component: AddDoctorPage,
      componentProps: {
        "doctor": doctor,
        "passedFrom": "addDoctor",
      }
    });
    await modal.present();
    modal.onDidDismiss().then(
      () => this.loadDoctorList()
    );
  }

  async viewDoctor(doctor: Doctor) {
    const modal = await this.modalController.create({
      component: AddDoctorPage,
      componentProps: {
        "doctor": doctor,
        "passedFrom": "viewDoctor",
      }
    });
    await modal.present();
    modal.onDidDismiss().then(
      () => this.loadDoctorList()
    );
  }
}
