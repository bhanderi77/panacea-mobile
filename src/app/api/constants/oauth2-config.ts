export const OAuthProviderClientCredentials = {
    profileUrl:
        'https://mobilepoc.erpnext.com/api/method/frappe.integrations.oauth2.openid_profile',

    tokenUrl:
        'https://mobilepoc.erpnext.com/api/method/frappe.integrations.oauth2.get_token',

    authServerUrl: 'https://mobilepoc.erpnext.com',

    authorizationUrl:
        'https://mobilepoc.erpnext.com/api/method/frappe.integrations.oauth2.authorize',

    revocationUrl:
        'https://mobilepoc.erpnext.com/api/method/frappe.integrations.oauth2.revoke_token',

    scope: 'all%20openid',

    clientId: '8ff5e8ca95',

    appUrl: 'http://localhost:8100',
};