import { Injectable } from '@angular/core';
import { Doctor } from '../model/doctor';
import { HttpClient } from '@angular/common/http';
import { TokenService } from '../api/token.service';
import { OAuthProviderClientCredentials } from '../api/constants/oauth2-config';
import { switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DoctorService {

  constructor(
    private http: HttpClient,
    private token: TokenService,
  ) { }

  addDoctor(doctor: Doctor) {
    const doc = {
      doctor_name: doctor.name,
      area: doctor.area,
      sub_territory: doctor.place,
      doctype: "Doctor",
    };

    return this.token.getToken().pipe(
      switchMap(token => {
        return this.http.post(
          OAuthProviderClientCredentials.authServerUrl + "/api/method/frappe.client.insert",
          { "doc": doc },
          {
            headers: {
              'Authorization': 'Bearer ' + token,
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            },
          }
        );
      })
    )
  }

  updateDoctor(doctor: Doctor) {
    console.log({doctor});
    const doc = {
      doctor_name: doctor.name,
      area: doctor.area,
      sub_territory: doctor.place,
      name: doctor.serverID,
      doctype: "Doctor",
    };

    return this.token.getToken().pipe(
      switchMap(token => {
        return this.http.post(
          OAuthProviderClientCredentials.authServerUrl + "/api/method/frappe.client.save",
          { "doc": doc },
          {
            headers: {
              'Authorization': 'Bearer ' + token,
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            },
          }
        );
      })
    )

  }

}
