import { TestBed } from '@angular/core/testing';

import { DailyCallReportService } from './daily-call-report.service';

describe('DailyCallReportService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DailyCallReportService = TestBed.get(DailyCallReportService);
    expect(service).toBeTruthy();
  });
});
