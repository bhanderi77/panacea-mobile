import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyActivityPage } from './daily-activity.page';

describe('DailyActivityPage', () => {
  let component: DailyActivityPage;
  let fixture: ComponentFixture<DailyActivityPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyActivityPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyActivityPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
