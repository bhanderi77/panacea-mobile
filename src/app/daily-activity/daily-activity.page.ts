import { Component, OnInit } from '@angular/core';
import { DailyCallReportService } from '../services/daily-call-report.service';
import { DailyActivity } from '../model/daily-activity';

@Component({
  selector: 'app-daily-activity',
  templateUrl: './daily-activity.page.html',
  styleUrls: ['./daily-activity.page.scss'],
})
export class DailyActivityPage implements OnInit {

  private dailyActivityList : Array<DailyActivity>;

  constructor(
    DCRService: DailyCallReportService,
  ) {
    this.dailyActivityList = DCRService.dailyActivityList;
  }

  ngOnInit() {
  }

}
