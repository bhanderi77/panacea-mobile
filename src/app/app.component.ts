import { Component } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { TokenService } from './api/token.service';
import { OAuthProviderClientCredentials } from './api/constants/oauth2-config';
import { Router } from '@angular/router';
import { APP_KEY } from './constants/strings';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public appPages = [
    {
      title: 'Dashboard',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Doctor Visit',
      url: '/doctor-visit',
      icon: 'people'
    },
    {
      title: 'Daily Activity',
      url: '/daily-activity',
      icon: 'today'
    },
    {
      title: 'Doctor',
      url: '/doctor',
      icon: 'medkit'
    },
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private token: TokenService ,
    private router: Router,
    private navController:NavController
  ) {
    this.initializeApp();
    this.backButtonEvent();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.token.setupOauthConfig(OAuthProviderClientCredentials);
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  backButtonEvent() {
    this.platform.backButton.subscribeWithPriority(1, () => {
      const url = this.router.url;
      if (url.match('(^/[a-zA-Z0-9-.]*)$')) {
        navigator[APP_KEY].exitApp();
      } else {
        this.navController.navigateBack(
          url.replace(new RegExp('(/([a-zA-Z0-9-.])*)$'), ''),
        );
      }
    });
  }

}
