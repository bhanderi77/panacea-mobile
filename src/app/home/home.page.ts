import { Component, OnInit } from '@angular/core';
import { TokenService } from '../api/token.service';
import { StorageService } from '../api/storage.service';
import { LOGGED_IN, ACCESS_TOKEN, REFRESH_TOKEN, EXPIRES_IN } from '../api/constants/storage';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  loggedIn: boolean = false;
  hideAuthButtons: boolean = false;

  constructor(
    private token: TokenService,
    private storage: StorageService,
  ) { }

  ngOnInit() {
    this.storage.changes.subscribe(event => {
      if (event.key === LOGGED_IN) {
        this.loggedIn = event.value ? true : false;
      }

      if (this.loggedIn) {
        const initialHref = window.location.href;
        navigator['splashscreen'].show();
        window.location.href = initialHref;
      }
    })
  }

  ionViewWillEnter() {
    this.loggedIn = localStorage.getItem(LOGGED_IN) ? true : false;
  }

  login() {
    this.hideAuthButtons = true;
    this.token.initializeCodeGrant();
  }

  logout() {
    this.loggedIn = false;
    this.token.revokeToken();
    this.storage.clear(LOGGED_IN);
    this.storage.clear(ACCESS_TOKEN);
    this.storage.clear(REFRESH_TOKEN);
    this.storage.clear(EXPIRES_IN);
  }

  moreOptions() {
  }
}
