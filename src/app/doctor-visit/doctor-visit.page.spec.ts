import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorVisitPage } from './doctor-visit.page';

describe('DoctorVisitPage', () => {
  let component: DoctorVisitPage;
  let fixture: ComponentFixture<DoctorVisitPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorVisitPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorVisitPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
