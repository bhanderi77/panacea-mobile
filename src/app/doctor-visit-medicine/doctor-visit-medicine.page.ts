import { Component, OnInit } from '@angular/core';
import { DailyCallReportService } from '../services/daily-call-report.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Doctor } from '../model/doctor';
import { Medicine } from '../model/medicine';
import { DailyActivity } from '../model/daily-activity';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-doctor-visit-medicine',
  templateUrl: './doctor-visit-medicine.page.html',
  styleUrls: ['./doctor-visit-medicine.page.scss'],
})
export class DoctorVisitMedicinePage implements OnInit {

  private selectedDoctor: Doctor;
  private count: number;
  private medicineList: Array<{ name: string; isChecked: boolean }>;
  constructor(
    private DCRService: DailyCallReportService,
    private route: ActivatedRoute,
    private router: Router,
    private alertController: AlertController,
  ) {
    this.count = 0;
    route.queryParams.subscribe((params) => {
      if (router.getCurrentNavigation().extras.state) {
        this.selectedDoctor = router.getCurrentNavigation().extras.state.doctor;
      }
    });
    this.medicineList = [
      { name: "ALPHADOL 0.25 MCG CAPSULE", isChecked: false },
      { name: "ALPHADOL 1 MCG CAPSULE", isChecked: false },
      { name: "EPOTRUST-10000 1ml PFS", isChecked: false },
      { name: "EPOTRUST-10000 1ml VIAL", isChecked: false },
      { name: "EPOTRUST-2000 0.5ml PFS", isChecked: false },
      { name: "EPOTRUST-2000 0.5ml VIAL", isChecked: false },
      { name: "EVERGRAF 0.25mg TABLET", isChecked: false },
      { name: "EVERGRAF 0.5mg TABLET", isChecked: false },
      { name: "FOSBAIT 250 MG 1X30 TABLET", isChecked: false },
      { name: "FOSBAIT 500 MG 1X30 TABLET", isChecked: false },
      { name: "FOSBAIT 500 MG 1X30 TABLET", isChecked: false },
      { name: "IMUZA TABLET", isChecked: false },
      { name: "K-Bait Powder", isChecked: false },
      { name: "MIMCIPAR 30 TABLET", isChecked: false },
      { name: "MIMCIPAR 60 TABLET", isChecked: false },
      { name: "MYCEPT 250mg CAPSULE", isChecked: false },
      { name: "MYCEPT 500mg TABLET", isChecked: false },
    ]
  }

  ngOnInit() {
  }

  async confirmPromotion() {
    let selectedMedicineList: Array<Medicine> = [];
    this.medicineList.forEach((medicine) => {
      if (medicine.isChecked === true) {
        this.count += 1;
        let tempMedicine = new Medicine();
        tempMedicine.name = medicine.name;
        selectedMedicineList.push(tempMedicine);
      }
    });
    const alert = await this.alertController.create({
      message: `<strong>${selectedMedicineList.length}</strong> products promoted , Press yes to continue for DCR entry.`,
      buttons: [
        {
          text: 'NO',
          cssClass: 'danger',
          handler: () => {
          }
        }, {
          text: 'YES',
          handler: () => {
            let dailyActivity = new DailyActivity();
            dailyActivity.doctor = this.selectedDoctor;
            // this.medicineList.forEach((medicine) => {
            //   if (medicine.isChecked === true) {
            //     let tempMedicine = new Medicine();
            //     tempMedicine.name = medicine.name;
            //     dailyActivity.medicineList.push(tempMedicine);
            //   }
            // });
            dailyActivity.medicineList = selectedMedicineList;
            this.DCRService.dailyActivityList.push(dailyActivity);
            this.router.navigate(['daily-activity']);
          }
        }
      ]
    });
    await alert.present();

  }

}
