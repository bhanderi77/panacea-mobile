import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {
  NativeHttpModule,
  NativeHttpBackend,
  NativeHttpFallback,
} from 'ionic-native-http-connection-backend';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { TokenService } from './api/token.service';
import { StorageService } from './api/storage.service';
import { HttpBackend, HttpXhrBackend, HttpClientModule } from '@angular/common/http';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallbackComponent } from './callback/callback.component';
import { AddDoctorPageModule } from './add-doctor/add-doctor.module';

@NgModule({
  declarations: [AppComponent, CallbackComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    AddDoctorPageModule,
    NativeHttpModule,
    HttpClientModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    TokenService,
    StorageService,
    InAppBrowser,
    {
      provide: HttpBackend,
      useClass: NativeHttpFallback,
      deps: [Platform, NativeHttpBackend, HttpXhrBackend],
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
